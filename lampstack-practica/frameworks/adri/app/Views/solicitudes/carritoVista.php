<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<?php $session = \Config\Services::session(); ?>
<?php if ($session->has('carro')): ?>
    <?php $carro = $session->get('carro'); ?>
<table class="table table-dark table-hover" id="myTable">
  <thead>
    <tr>
      <th scope="col">NIE/NIF</th>
      <th scope="col">Solicitante</th>
      <th scope="col">Email</th>
      <th scope="col">Ciclo</th>
      <th scope="col">Matricula</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach ($carro as $solicitud): ?>
    <tr>
        <td><?=$solicitud['nif']?></td>
        <td><?=$solicitud['nombre']?></td>
        <td><?=$solicitud['email']?></td>
        <td><?=$solicitud['ciclo']?></td>
        <td><?=$solicitud['tipo_tasa'] == 1 ? 'ordinaria' : ($solicitud['tipo_tasa'] == 3 ? 'gratuita' : 'semigratuita')?></td>

    </tr>
<?php endforeach; ?>
  </tbody>
</table>

<!-- esto es para poner los botones bajo la tabla de la compra -->
<a href="<?= site_url('carritoController/borraCarro')?>"><button class="btn btn-danger">Vaciar carro</button></a>
<a href="<?= site_url('/')?>"><button class="btn btn-success">Seguir comprando</button></a>

    <?php else : ?>
    <h3>No hay artículos</h3>
    <p>El carro está vacio</p>
<?php endif ?>
<?= $this->endSection() ?>


<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<div class="container">
    <div>
        <?php if (isset($errors) AND !empty($errors->getErrors())) : ?>
            <div class="alert alert-danger">
                <?php foreach ($errors->getErrors() as $field => $error) : ?>
                    <p><?= $error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
    </div>

<?= form_open(site_url('pauController/afegir'))?>
    <div class="mb-3">
        <?= form_label('NIF/NIE','nif',['class'=>'form-label'] )?>
        <?= form_input('nif',set_value('nif'),['class'=>'form-control', 'id'=>'nif'])?>
    </div>
    <div class="mb-3">
        <?= form_label( 'Nombre','nombre',['class'=>'form-label'] )?>
        <?= form_input('nombre',set_value('nombre'),['class'=>'form-control', 'id'=>'nombre'])?>
    </div>
    <div class="mb-3">
        <?= form_label( '1er Apellido','apellido1',['class'=>'form-label'] )?>
        <?= form_input('apellido1',set_value('apellido1'),['class'=>'form-control', 'id'=>'apellido1'])?>
    </div>
    <div class="mb-3">
        <?= form_label( '2º Apellido','apellido1',['class'=>'form-label'] )?>
        <?= form_input('apellido2',set_value('apellido2'),['class'=>'form-control', 'id'=>'apellido2'])?>
    </div>
    <div class="mb-3">
        <?= form_label( 'Ciclo','ciclo',['class'=>'form-label'] )?>
        <?= form_dropdown('ciclo',$ciclos, set_value('ciclo'),['class'=>'form-control', 'id'=>'ciclo'])?>
    </div>

    <div class="mb-3">
        <?= form_label( 'Email','email',['class'=>'form-label'] )?>
        <?= form_input('email',set_value('email'),['class'=>'form-control', 'id'=>'email'])?>
    </div>
    <div class="mb-3">
        <?= form_label( 'Repetir Email','email1',['class'=>'form-label'] )?>
        <?= form_input('email1',set_value('email1'),['class'=>'form-control', 'id'=>'email1'])?>
    </div>
    <div class="form-check form-check-inline mb-3">
        <?= form_radio('tipo_tasa', '1', set_radio('tipo_tasa','1', TRUE), ['class'=>'form-check-input', 'id'=>'ordinaria'] )?>
        <?= form_label('Ordinaria','ordinaria',['class'=>'form-check-label'])?>
    </div>
    <div class="form-check form-check-inline mb-3">
        <?= form_radio('tipo_tasa', '2', set_radio('tipo_tasa','2',FALSE), ['class'=>'form-check-input', 'id'=>'semigratuita'] )?>
        <?= form_label('Semigratuita','semigratuita',['class'=>'form-check-label'])?>
    </div>
    <div class="form-check form-check-inline mb-3">
        <?= form_radio('tipo_tasa', '3', set_radio('tipo_tasa','3',FALSE), ['class'=>'form-check-input', 'id'=>'gratuita'] )?>
        <?= form_label('Gratuita','gratuita',['class'=>'form-check-label'])?>
    </div>
    <div class="mb-3">
        <?= form_submit('boton','Regístrate',["class"=>"btn btn-primary"]) ?>
    </div>
</div>
<?= form_close()?>
<?= $this->endSection() ?>





<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<?php
$auth = new \IonAuth\Libraries\IonAuth();
?>

<table class="table table-dark table-hover" id="myTable">
  <thead>
    <tr>
      <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))): ?>
      <th scope="col">NIE/NIF</th>
      <?php endif; ?>
      <th scope="col">Solicitante</th>
      <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))): ?>
      <th scope="col">Email</th>
      <?php endif; ?>
      <th scope="col">Ciclo</th>
      <th scope="col">Matricula</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach ($solicitudes as $solicitud): ?>
    <tr>
        <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))): ?>
        <td><?=$solicitud['nif']?></td>
        <?php endif; ?>
        <td><?=$solicitud['solicitante']?></td>
        <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))): ?>
        <td><?=$solicitud['email']?></td>
        <?php endif; ?>
        <td><?=$solicitud['ciclo']?></td>
        <td><?=$solicitud['tipo_tasa'] == 1 ? 'ordinaria' : ($solicitud['tipo_tasa'] == 3 ? 'gratuita' : 'semigratuita')?></td>
        <td>
            <?php if ($auth->loggedIn() AND ($auth->isAdmin() OR $auth->inGroup('secretaria'))): ?>
            <a href="<?= site_url('pauController/borrar/' . $solicitud['id']) ?>"
               onclick="return confirm('Vas a borrar a <?= $solicitud['solicitante'] ?>')"><button class="btn btn-danger">Borrar</button></a>
            <?php endif; ?>
            <a href="<?= site_url('carritoController/comprar') ?>"><button class="btn btn-success">Comprar</button></a>
        </td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
<?= $this->endSection() ?>
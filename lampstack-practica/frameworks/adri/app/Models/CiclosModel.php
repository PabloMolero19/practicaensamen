<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of pauModel
 *
 * @author jose
 */
class CiclosModel extends Model {
    protected $table      = 'ciclos';
    protected $primaryKey = 'id';
    protected $returnType = 'array';
}

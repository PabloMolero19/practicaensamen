<?php

namespace App\Controllers;

use App\Models\PauModel;
use App\Models\CiclosModel;
use Config\Services;

class CarritoController extends BaseController {
    
    protected $session;
    protected $datos;
    // aquí se añade al carro. CAMBIAR EL ID = 1. QUITAR LA IGUALDAD
    public function comprar($id = 1){

        //Comprobar si el artículo está en el carro
       if ($this->session->has('carro')) {
           //existe el carro
           $carro = $this->session->get('carro');
           if (!isset($carro[$id])){
            $solicitudes = $this->datos->find($id);
            //no existe el elmento. Camiar el array al gusto, en nuestro caso es solicitudes porque AGRUPA TODO EL CONTENIDO
            $solicitudes ['id'] = 1;
            $carro[$id]=$solicitudes; //añadimos un elemento al array
           }
       } else {
           //crear el carro
           $solicitudes = $this->datos->find($id);
           $carro[$id]=$solicitudes;
           $solicitudes ['id'] = 1;
       }
       
       //y en todos los casos guardar la variable de sesion, la nuestra se llama carro
       $this->session->set('carro',$carro);
       return redirect()->to(site_url('/pauController'));
    }
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        //--------------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //--------------------------------------------------------------------
        $this->datos = new PauModel();
        $this->session = Services::session();
    }

    public function verCarrito(){
        $data['titulo'] = "Carro";
        $data['header'] = "Carrito";
        return view('solicitudes/carritoVista',$data);
    }
    
    public function borraCarro(){
        $this->session->remove('carro');
        return redirect()->to(site_url('/'));
    } 
}
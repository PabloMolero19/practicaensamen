<?php
namespace App\Controllers;

use App\Models\PauModel;
use App\Models\CiclosModel;
use Config\Services;

$session = \Config\Services::session();

class PauController extends BaseController {
    
    protected $auth;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        //--------------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //--------------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();
    } 
    
    //put your code here
    public function index(){
        $pauModel = new PauModel();
        $data['titulo'] = "Home";
        $data['header'] = "Home";
        $data['solicitudes'] = $pauModel
                ->select("nif, CONCAT(pau.apellido1,' ',pau.apellido2,', ',pau.nombre) as solicitante, email, pau.id, email, ciclos.nombre, ciclo, ciclos.id as id_ciclo, familia, tipo_tasa")
                ->join('ciclos','ciclos.id=pau.ciclo','LEFT')
                ->findAll();
        return view('solicitudes/lista',$data);
    }
    
    public function borrar($id){
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
            $pauModel = new PauModel();
            $pauModel->delete($id);
            return redirect()->to('/pauController');
        } else {
            
            return view('solicitudes/noPuedes');
        }
    }
    
    public function afegir(){
        helper(['form','myarray']);
        $pauModel = new PauModel();
        $ciclosModel = new CiclosModel();
        $data['titulo'] = "Registro";
        $data['header'] = "¡Regístrate!";
        if ($this->request->getMethod() == "post") { //viene de un formulario
             $reglas = $pauModel->getValidationRules();
             $reglas['email'].='|matches[email1]';
             $reglas['email1']='required|valid_email';
             if ($this->validate($reglas)){
                 $solicitud = $this->request->getPost();
                 unset($solicitud['email1']);
                 unset($solicitud['boton']);
                 $pauModel->insert($solicitud);
                 return redirect()->to('/pauController');
                 //print_r($solicitud);
             } else {
                 //mostrar formulario
                 
                 $data['errors'] = $this->validator;
             }
        } else { //viene de una URL
           //mostrar formulario 
        }
        $ciclos=$ciclosModel->select('id,nombre')
                ->where(['grado'=>'superior'])
                ->findAll();
        $data['ciclos'] = changeArray($ciclos, 'id', 'nombre');
        return view('solicitudes/form',$data);
    }
}

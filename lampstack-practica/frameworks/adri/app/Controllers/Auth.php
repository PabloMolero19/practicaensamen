<?php 

namespace App\Controllers;
use Config\Services;
use App\Models\PauModel;

class Auth extends \IonAuth\Controllers\Auth
{
     protected $viewsFolder = 'auth';
     
    protected $auth;
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
    parent::initController($request, $response, $logger);
    $this->datos = new PauModel;
    $this->session = Services::session();
    $this->auth = new \IonAuth\Libraries\IonAuth();
    }
    public function new() {
        if (!$this->auth->loggedIn())
        {return redirect()->to(site_url('auth/login'));
    }
    }

}
